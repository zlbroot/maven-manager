package com.lambo.maven.core.utils;

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Test;

public class HttpUtilTest {

    @Test
    public void doGet() throws IOException {
        byte[] data = HttpUtil.doGet("https://www.baidu.com");
        assert data != null;
        System.out.write(data);
    }

    @Test
    public void doGet2() throws IOException {
        String url = "http://maven.aliyun.com/nexus/content/groups/public/org/springframework/boot/spring-boot-maven-plugin/maven-metadata-public.xml";
        byte[] data = HttpUtil.doGet(url);
        assert data != null;
        System.out.write(data);
    }

    @Test
    public void doGet3() throws IOException {
        String url = "https://archiva-maven-storage-prod.oss-cn-beijing.aliyuncs.com/repository/central/org/springframework/boot/spring-boot-maven-plugin/maven-metadata-public.xml?Expires=1659274761&OSSAccessKeyId=LTAIfU51SusnnfCC&Signature=i1qpqDNtxM58Wfcfh2vPnRu%2ByRQ%3D";
        byte[] data = HttpUtil.doGet(url);
        assert data != null;
        System.out.write(data);
    }
}