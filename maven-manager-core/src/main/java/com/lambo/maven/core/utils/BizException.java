package com.lambo.maven.core.utils;

/**
 * 简单的业务异常
 *
 * @author 林小宝
 * @createTime 2014年8月10日 上午1:01:48
 */
public class BizException extends RuntimeException {
    private static final long serialVersionUID = 6368601197928340154L;
    private int code = -1;

    public BizException(int code, String message) {
        super(message);
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
