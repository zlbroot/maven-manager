package com.lambo.maven.core.utils;

import java.util.Collection;
import java.util.Map;

/**
 * 断言用于验证参数.
 */
public abstract class AssertUtil {

    /**
     * 为真验证.
     * @param expression 表达式.
     * @param message 异常消息.
     */
    public static void isTrue(boolean expression, String message) {
        if (!expression) {
            throw new IllegalArgumentException(message);
        }
    }

    /**
     * 非空验证.
     * @param object 对象.
     * @param message 异常消息.
     */
    public static void notNull(Object object, String message) {
        if (object == null) {
            throw new IllegalArgumentException(message);
        }
    }
}
